# Change robot.tmLanguage: |Preconditions|Procedure|Postconditions line 73
# I have added custom separators in test cases for doors cells

# The idea is to have a single file (.txt) where the complete test case can be described. A tool can
#  then parse it and separate it in preconditions, procedure, e. result, and postcondition and numerate them.
#
# Main features:
#
# * Dependent on one file
# * Only one procedure
# * No numbering is needed
# * Easy to edit on sublime
# * Text highlighting can be used
# * Scripts can create files with this format easily
#
# Ver por ejemplo [robot framework](https://github.com/robotframework/QuickStartGuide/blob/master/QuickStart.rst#robot-framework-overview)
#
# [Plugin de sublime](https://github.com/andriyko/sublime-robot-framework-assistant)

*** Keywords ***
  set_voltage
  read_voltage

*** Test Cases ***
  [Documentation]r
  [Preconditions]
  set_voltage     5
  [Procedure]
  set_voltage     5
  read_voltage    5
  rqst_data1      5    5
  [Postconditions]
  set_voltage     0
