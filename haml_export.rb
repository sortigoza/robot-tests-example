require 'csv'
require 'haml'
require 'nokogiri'

module TCAL
  class TestSuite

    attr_accessor :tests

    def initialize xml_exp_tests
      engine = Haml::Engine.new(xml_exp_tests)
      xml_exp_tests  = Nokogiri::XML(engine.render)
      load_test_cases xml_exp_tests
    end

    def to_explicit_procedure
      @tests.inject("") { |str, a| str += "#{a.full_procedure}\n" }
    end

    def to_doors_csv
      DoorsExporter.new(self).csv
    end

    private

    def load_test_cases xml_exp_tests
      @tests = xml_exp_tests.xpath("//test").collect { |node| TestCase.new(node) }
    end

  end

  class TestCase

    attr_accessor :preconditions, :explicit_procedure, :postconditions

    def initialize xml_exp_test
      xml_doc  = xml_exp_test
      get_preconditionss xml_doc
      get_explicit_procedure xml_doc
      get_postconditions xml_doc
    end

    def full_procedure
      procedure = ""
      @preconditions.each { |a| procedure += "pre.#{a.to_text}\n" }
      @explicit_procedure.each { |a| procedure += "proc.#{a.to_text}\n" }
      @postconditions.each { |a| procedure += "post.#{a.to_text}\n" }
      procedure
    end

    def procedure
      @explicit_procedure.collect do |p|
        next "#{p.number})#{p.instruction[/^\w+[^\|]/]}" if p.read?
        next "#{p.number})#{p.instruction[/^\w+[^\|]/]} = #{p.parameters(:procedure).join '&'}" if p.rqst?
        "#{p.number})#{p.instruction[/^\w+[^\|]/]} = #{p.parameters.join '&'}"
      end
    end

    def e_results
      @explicit_procedure.collect do |p|
        next "#{p.number})#{p.signal} = #{p.parameters.join '&'}" if p.read?
        next "#{p.number})#{p.signal} = #{p.parameters(:result).join '&'}" if p.rqst?
      end
    end

    private

    def get_preconditionss xml_doc
      @preconditions = get_node_data(xml_doc, "prec")
    end

    def get_explicit_procedure xml_doc
      @explicit_procedure = get_node_data(xml_doc, "exp_p")
    end

    def get_postconditions xml_doc
      @postconditions = get_node_data(xml_doc, "post")
    end

    def get_node_data xml_doc, node
      result = []
      data = xml_doc.css(node).text
      data.split(/\n/)
      .map! { |l| l.lstrip }
      .reject { |c| c.empty? || c =~ /^ *$/ }
      .each_with_index { |a, i| result << Step.new(i + 1, a) }
      result
    end

  end

  class Step

    INSTRUCTION_REGEX = %r{^(\w+) *(.*)$}
    COMMENT_REGEX = %r{^#}

    READ_REGEX = %r{read_(\w+)}i
    RQST_REGEX = %r{rqst_(\w+)}i
    SET_REGEX = %r{set_(\w+)}i
    SIGNAL_REGEX = %r{(read_|rqst_|set_)(\w+)}i

    attr_accessor :number, :instruction

    def initialize number, instruction_params
      @number = number
      match = instruction_params.match(INSTRUCTION_REGEX)
      @instruction = match.captures[0]
      @parameters = match.captures[1].split
    end

    def to_text
      "#{@number})#{@instruction} = #{@parameters.join ' '}"
    end

    def parameters side=nil
      return @parameters.join(' ')[/^(.*)\|/, 1].split if side == :procedure
      return @parameters.join(' ')[/\|(.*)$/, 1].split if side == :result
      @parameters
    end

    def read?
      @instruction =~ READ_REGEX
    end

    def rqst?
      @instruction =~ RQST_REGEX
    end

    def set?
      @instruction =~ SET_REGEX
    end

    def signal
      @instruction[SIGNAL_REGEX, 2]
    end

  end

  class DoorsExporter

    attr_accessor :csv

    def initialize test_suite
      @csv = CSV.generate do |csv|
        test_suite.tests.each do |t|
          csv << [format_preconditions(t), f_procedure(t), f_expected(t), f_postconditions(t)]
        end
      end
    end

    def format_preconditions test
      test.preconditions.inject("") { |text, a| text += "#{a.instruction} = #{a.parameters.join '&'}\n"}
    end

    def f_procedure test
      test.procedure.inject("") { |text, a| text += "#{a}\n" }
    end

    def f_expected test
      test.e_results.inject("") { |text, a| text += "#{a}\n" }
    end

    def f_postconditions test
      test.postconditions.inject("") { |text, a| text += "#{a.instruction} = #{a.parameters.join '&'}\n"}
    end

  end
end

haml_str = File.read('tests.haml')
ts = TCAL::TestSuite.new(haml_str)

puts ts.to_explicit_procedure
puts ts.to_doors_csv

puts TCAL::DoorsExporter.new(ts).csv

